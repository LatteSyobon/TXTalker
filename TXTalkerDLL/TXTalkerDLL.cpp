﻿// TXTalkerDLL.cpp : DLL 用にエクスポートされる関数を定義します。
//

#include "pch.h"
#include "framework.h"
#include "TXTalkerDLL.h"

#include "txta.h"
#include "../TXTalker/config.h"
#include "../TXTalker/TXTalker.cpp"



// これは、エクスポートされた変数の例です
TXTALKERDLL_API int nTXTalkerDLL=0;

// これは、エクスポートされた関数の例です。
TXTALKERDLL_API int fnTXTalkerDLL(void)
{
    return 0;
}

// これは、エクスポートされたクラスのコンストラクターです。
CTXTalkerDLL::CTXTalkerDLL()
{
    return;
}

void print_index(void)
{
	std::string index[100];
	std::string path = load();
	int array_num = 0;
	array_num = txttorawarray_str(path, index);
	printf("%s\n", index[random(0, array_num)].c_str());
}
