﻿// 以下の ifdef ブロックは、DLL からのエクスポートを容易にするマクロを作成するための
// 一般的な方法です。この DLL 内のすべてのファイルは、コマンド ラインで定義された TXTALKERDLL_EXPORTS
// シンボルを使用してコンパイルされます。このシンボルは、この DLL を使用するプロジェクトでは定義できません。
// ソースファイルがこのファイルを含んでいる他のプロジェクトは、
// TXTALKERDLL_API 関数を DLL からインポートされたと見なすのに対し、この DLL は、このマクロで定義された
// シンボルをエクスポートされたと見なします。
#ifdef TXTALKERDLL_EXPORTS
#define TXTALKERDLL_API __declspec(dllexport)
#else
#define TXTALKERDLL_API __declspec(dllimport)
#endif

// このクラスは dll からエクスポートされました
class TXTALKERDLL_API CTXTalkerDLL {
public:
	CTXTalkerDLL(void);
	// TODO: メソッドをここに追加します。
};

extern TXTALKERDLL_API int nTXTalkerDLL;

TXTALKERDLL_API void print_index(void);
