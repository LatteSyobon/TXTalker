#include <fstream>
#include <iostream>

#include "config.h"

std::string load()
{
    std::string str;
    std::fstream file("./tk.config");

    int array_num = 0;

    if (file.fail()) {
        std::cerr << "Cannot find 'tk.config'." << std::endl;
        std::cerr << "To load the file, enter the filename in 'tk.config' on the first line" << std::endl;
        exit(0);
    }
    getline(file, str);
	return std::string(str);
}
