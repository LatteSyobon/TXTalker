﻿// TXTalker.cpp : このファイルには 'main' 関数が含まれています。プログラム実行の開始と終了がそこで行われます。
//

#include <stdio.h>
#include <string>
#include <random>
#include <fstream>
#include "txta.h"
#include "config.h"

std::random_device rd;
std::mt19937 gen(rd());

int random(int low, int high)
{
	std::uniform_int_distribution<> dist(low, high);
	return dist(gen);
}


int main() {
	std::string index[100];
	std::string path = load();
	int array_num = 0;
	array_num = txttorawarray_str(path, index);
	printf("%s\n", index[random(0, array_num)].c_str());
}